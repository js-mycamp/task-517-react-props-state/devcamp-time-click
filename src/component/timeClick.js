

import { Component } from "react";

class Time extends Component {
    constructor(props) {
        super(props);
        this.state = {
            time: new Date(),
            timeArray: [],
        };
    }

    handleAddToList = () => {
        this.setState((prevState) => ({
            timeArray: [...prevState.timeArray, new Date()],
        }));
    };
    render() {
        return (
            <>
                <button onClick={this.handleAddToList}>Add to List</button>
                <ul>
                    {this.state.timeArray.map((item, index) => (
                        <li key={index}>{item.toLocaleTimeString()}</li>
                    ))}
                </ul>

            </>
        );
    }
}

export default Time;
